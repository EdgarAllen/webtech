package edgarallen.webtech.blocks.tileentities;

import edgarallen.webtech.fsm.FiniteStateMachine;
import net.minecraft.tileentity.TileEntity;

public class TileEntityStateController extends TileEntity {

    private FiniteStateMachine fsm;

    public FiniteStateMachine getFsm() { return fsm; }

}