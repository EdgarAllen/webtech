package edgarallen.webtech.blocks;

import edgarallen.webtech.WebTech;
import edgarallen.webtech.blocks.tileentities.TileEntityStateController;
import edgarallen.webtech.lib.GuiIDs;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockStateController extends Block implements ITileEntityProvider {
	public static final String NAME = "state_controller";

	public BlockStateController() {
		super(Material.iron);

		setUnlocalizedName(NAME);
		setRegistryName(NAME);
		setHardness(2.5f);
		setResistance(5.0f);
		setHarvestLevel("pickaxe", 0);
        setCreativeTab(CreativeTabs.tabRedstone);
	}

	@SideOnly(Side.CLIENT)
	public void initModels() {
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ) {
		if(!worldIn.isRemote) {
			TileEntity tile = worldIn.getTileEntity(pos);
			if(tile instanceof TileEntityStateController) {
				playerIn.openGui(WebTech.instance, GuiIDs.STATE_CONTROLLER, worldIn, pos.getX(), pos.getY(), pos.getZ());
			}
		}

		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityStateController();
	}
}