package edgarallen.webtech.lib;

import edgarallen.webtech.blocks.BlockStateController;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModBlocks {
	public static BlockStateController blockStateController;

	public static void init() {
		ModBlocks.blockStateController = new BlockStateController();
		GameRegistry.registerBlock(ModBlocks.blockStateController);
	}

	@SideOnly(Side.CLIENT)
	public static void registerClientModels() {
		ModBlocks.blockStateController.initModels();
	}
}