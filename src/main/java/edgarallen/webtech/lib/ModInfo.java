package edgarallen.webtech.lib;

public class ModInfo {
	public static final String ID = "webtech";
	public static final String NAME = "WebTech";
	public static final String VERSION = "@VERSION@";
	public static final String CLIENT_PROXY = "edgarallen.webtech.network.proxy.ClientProxy";
	public static final String SERVER_PROXY = "edgarallen.webtech.network.proxy.ServerProxy";
	public static final String NETWORK_CHANNEL = "webtech";
}