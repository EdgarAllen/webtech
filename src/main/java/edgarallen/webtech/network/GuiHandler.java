package edgarallen.webtech.network;

import edgarallen.webtech.lib.GuiIDs;
import edgarallen.webtech.gui.containers.ContainerStateMachine;
import edgarallen.webtech.gui.GuiStateController;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if(ID == GuiIDs.STATE_CONTROLLER) {
			return new ContainerStateMachine();
		}

		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if(ID == GuiIDs.STATE_CONTROLLER) {
			return new GuiStateController();
		}

		return null;
	}
}