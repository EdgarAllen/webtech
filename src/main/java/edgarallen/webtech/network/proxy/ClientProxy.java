package edgarallen.webtech.network.proxy;

import edgarallen.webtech.lib.ModBlocks;

public class ClientProxy implements ISidedProxy {

	@Override
	public void preInit() {
		ModBlocks.registerClientModels();
	}
}