package edgarallen.webtech.gui;

import edgarallen.webtech.gui.containers.ContainerStateMachine;
import net.minecraft.client.gui.inventory.GuiContainer;

public class GuiStateController extends GuiContainer {

	public GuiStateController() {
		super(new ContainerStateMachine());
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {

	}
}